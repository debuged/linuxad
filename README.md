# LinuxAd

## Name
Installation und Integration von Ubuntu 20.04 LTS als Active Directory Client sowie
Administration mittels Gruppenrichtlinien.

## Beschreibung
In dieser Beschreibung wird gezeigt wie man ein Ubuntu installiert und einer Domäne beitritt. Zusätzlich wird der root Zugang mittels sudoers limitiert dieser wird dann nurnoch durch eine Gruppenzugehörigkeit im Active Directory verwaltet.

## Voraussetzung
Ubuntu verwendet 2 Netzwerkadapter mit:

1) Zugang zum Internet "Internet"
    - mit DHCP
    - dns ist nicht notwendig
2) Zugang zum AD-Netz "Corpnet"
    - statische IP
    - Im AD-Netz ist ein funktionstüchtiges Active Directory

|                         |                              |
| ----------------------- | ---------------------------- |
| Domäne-Name             | corp.js.com                  |
| Domäne-Controller-FQDN  | dc01.corp.js.com             |
| Domäne-Controller-IP    | 192.167.1.10                 |
| Domäne-Sichereitsgruppe | Server-Administratoren-Linux |
| Domäne-User             | linux_adm                    |




## Installation

### VM
In diesem Beispiel wird Hyper-V verwendet, als Konfiguration verwenden wir "Generation 2" und deaktivieren Secure Boot für die Installation.
Secure Boot wird automatisch nach der Installation wieder aktiviert. Bitte Recherchiren Sie wie man Secure Boot bei Hyper-V deaktivieren kann.

![Hyper-V Einstellungen](./img/01_Hyper-V_Settings.png)

### Boot
Zuerst wählen Sie das Menü Ubuntu Ausprobieren,
Um vor der Installation das Netzwerk zu konfigurieren
![Ubuntu Boot Auswahl](./img/Ubuntu_Boot_select.png)

### Network
Unter Setting: → Network

Müssen Sie das LAN Konfigurieren.

Wichtig: Nach der Änderungen muss Netzwerk aus und wieder ein geschaltet werden, damit Änderungen aktuallisiert werden.
![Ubuntu Netzwerk Konfiguration](./img/Network_Configuration.png)

Internet Adapter
* mit Deaktiviertem DNS:

  ![Ubuntu IP Konfiguration für Internet Adapter](./img/Network_Configuration_Internet_IP.png)

Internet Adapter Resultat:

![Ubuntu Resultat für Internet Adapter](./img/Network_Configuration_Internet_result.png)

Corpnet Adapter:
* Ohne DHCP
* DNS = 192.167.1.10, 8.8.8.8
* 192.167.1.10 ist Unser DNS und Domain Controller
* Ohne Gateway

![Ubuntu IP Konfiguration für Corpnet Adapter](./img/Network_Configuration_Corpnet_IP.png)

Corpnet Adapter Resultat:

![Ubuntu Resultat für Corpnet Adapter](./img/Network_Configuration_Corpnet_result.png)

Testen Sie die Verbindung zum Domänen Controller:

![Ubuntu Testen der DC Verbindung](./img/Network_ping_dc.png)


Nun setzten Wir den FQDN ad04.corp.js.com


![Ubuntu Install set FQDN](img/Adminuser_FQDN.png)


## Active Directory Konfiguration unter Windows

### Anlegen der Sichereitsgruppe und Benutzer im AD

Wärend der Linux Installation legen wir auf dem AD dem DC die Domäne-Sichereitsgruppe "Server-Administratoren-Linux" an.


![AD Erstellen Sichereitsgruppe](img/Adminuser_FQDN.png)



![AD Erstellen Sichereitsgruppe](img/AD_Create_security_grp.png)


Anschlißend den Domäne-User "linux_adm"

![AD Erstellen Admin User](img/AD_Create_linux_admin_user.png)


### Ändern der Gruppenzugehörigkeit für Domäne-User
Dieser Schritt hat eine besondere Bedeutung.
Von dieser Gruppenzugehörigkeit werden die lokalen Administrationsrechte im Linux abgeleitet werden!

![AD Gruppenzugehörigkeit](./img/AD_User_to_group.png)


## Nach Installation und Domänen Beitrit

### Fehlenden Komponenten

In einem Terminal werden die Fehlenden System Komponenten nachinstalliert installiert:

```bash
sudo apt install -y adcli realmd sssd-tools git
```

Anschließend muss das System neugestartet werden.

### Domänen Beitrit
Über die Einstellungen -> Unter Benutzer -> Unlock 

Durch das Ebsperren ist es uns erlaubt Benutzer accounts zu erzeugen.

![Freischalen](./img/Unlock_User.png)

Über Benutzer Hinzufügen fügen -> Unternehmensanmeldung/Enterprise Login.

![Enterprise Login](./img/Enterprise_Login.png)

Treagen wir unsere anmelde daten ein.

![AD Gruppenzugehörigkeit](./img/Add_User.png)

Nach dem erfogreichen anmelden folgt ein reboot.

### Testen des Domänen Anmeldung
Wir weckseln den User und testen Unser login.

![AD Gruppenzugehörigkeit](./img/Switch_User.png)

### Einrichen des User Accounts
Der Domänen User hat noch kein richtges Wurzel Vezeichniss.
Wir weckseln zurück in das Alte Account (ad04) und öffnen ein neues terminal.

```bash
sudo mkdir ../linux_adm@corp.js.com
sudo cp ./bash* ../linux_adm@corp.js.com/
sudo cp ./profile ../linux_adm@corp.js.com/
sudo login linux_adm@corp.js.com
sudo chown $(id -u):$(id -g) -R ../linux_adm@corp.js.com
exit
```

### Einrichen der super user Berechtigungen
Damit der linux_adm User als root werden kann, müssen wir ihm entprechende 
Eskalation-Prvilegein geben. Mit unserem ad04 user füren wir folgende Befehle aus.

```bash#
git clone https://gitlab.com/debuged/linuxad.git
sudo mkdir -p /etc/sudoers.d/
sudo cp linuxad/linuxad.sudoers /etc/sudoers.d/
```



sudo -u linux_adm@corp.js.com id
deluser ad04 sudo
usermod linux_adm@corp.js.com -G sudo
usermod ad04 -G sudo


Gegebenfalls kann
Das heist ist linux_adm mitglied von Server-Administratoren-Linux ist hat er Administrator rechte.
Ist linux_adm nicht mitglied von Server-Administratoren-Linux kann er nicht  Administrator rechte bekommen.


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.



* Profil roaming ?
adk : userstate migration tools USMT (scanstate loadstate)
azure: enterprise roaming



